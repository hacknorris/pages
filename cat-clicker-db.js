catFiles = [
    "oneko-normal-white.gif",
    "oneko-normal-grey.gif",
    "oneko-normal-yellow.gif",
    "oneko-normal-orange.gif",
    "oneko-normal-brown.gif",
    "oneko-color-orange.gif",
    "oneko-color-yellow.gif",
    "oneko-color-pink.gif",
    "oneko-color-blue.gif",
    "oneko-color-green.gif",
    "oneko-white-black.gif",
    "oneko-white-orange.gif",
    "oneko-white-grey.gif",
    "oneko-white-yellow.gif",
    "oneko-white-blue.gif",
    "oneko-neon-grey.gif",
    "oneko-neon-orange.gif",
    "oneko-neon-pink.gif",
    "oneko-neon-blue.gif",
    "oneko-neon-green.gif",
    "oneko-special-horror.gif",
    "oneko-special-ghost.gif",
    "oneko-special-rainbow.gif",
    "oneko-special-robot.gif",
    "oneko-special-negative.gif"
];
themes = {
    "normal-blue": 100,
    "normal-red": 250,
    "normal-yellow": 500,
    "light-blue": 1000,
    "light-purple": 2500,
    "light-green": 5000,
    "special-grayscale": 10000,
    "special-rainbow": 25000,
    "special-hotdog": 50000
};
cats = {
    "default-white":0,
    "default-grey":10,
    "default-yellow":20,
    "default-orange":40,
    "default-brown": 80, //end of first section
    "color-orange": 100,
    "color-yellow": 250,
    "color-pink": 500,
    "color-blue": 1000,
    "color-green": 2500, //end of second section
    "shine-black": 5000,
    "shine-orange": 7500,
    "shine-grey": 10000,
    "shine-yellow": 12500,
    "shine-blue": 15000, //end of third section
    "neon-grey": 20000,
    "neon-orange": 40000,
    "neon-pink": 80000,
    "neon-blue": 160000,
    "neon-green": 320000, //end of fourth section
    "special-horror": 1000000,
    "special-ghost": 2000000,
    "special-rainbow": 4000000,
    "special-robot": 8000000,
    "special-atom": 16000000//end of cats
};
lasers = {
    "red":0,
    "orange": 10,
    "yellow":  20,
    "green": 40,
    "blue": 80,
    "purple": 160,
    "pink": 320,
    "white": 640,
    "black": 1280,
    "grey": 2560,
    "gold": 5120,
    "silver": 10240,
    "rainbow": 20480,
    "tiktok": 40960,
    "double": 81920,
    "triple": 163840
};
boosts = {
    "coffee": [10, 1800000], // half hour
    "pet": [100, 10800000], // 3 hours
    "sleep": [1000, 28800000], // 8 hours
    "holiday": [10000, 60480000] // week
};