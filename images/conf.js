folders = {
	"animated":[
		"birb_profile.gif",
		"city_days_passing.gif",
		"easter_egg.gif",
		"girl-kde.gif",
		"guy_running.gif",
		"hello.gif",
		"pika-kde.gif",
		"portaler.gif",
		"red-kde.gif",
		"stickman_goes_out_home.gif",
		"tower_lospec_anim.gif",
		"mainboard_console.gif"
	],
	"emojis":[
		"bot_emoji_transparent.png",
		"drgns.png",
		"EEPY.png",
		"falling.png",
		"game_over.png",
		"happy.png",
		"SHAKED.png",
		"skewed.png"
	],
	"game_ideas":[
		"1_egg_nest.jpeg",
		"2_eggs_nest.jpeg",
		"bathroom.PNG",
		"bedroom.PNG",
		"camping.png",
		"changing_room.PNG",
		"city_park.png",
		"city_street.png",
		"empty_nest.jpeg",
		"fishing_place.png",
		"gold_egg_nest.PNG",
		"gym.PNG",
		"gym_school.PNG",
		"kitchen.PNG",
		"lift.PNG",
		"living_room.PNG",
		"outside_city.png",
		"restaurant.PNG",
		"school.PNG",
		"shop.PNG",
		"workplace.PNG"
	],
	"generic_pixels":[
		"4-puzzle.png",
		"anubi.png",
		"backroom_iso.png",
		"birb.png",
		"black_cat.png",
		"burgar.png",
		"cat.png",
		"champagne.png",
		"codeberg.png",
		"console.png",
		"dragon_selfie.png",
		"drink.png",
		"dude_id.png",
		"fish.png",
		"flower.png",
		"fox_balled.png",
		"fridge.png",
		"gnome.png",
		"magic_qr's.png",
		"me.png",
		"me_old.png",
		"neon_palm.png",
		"pool_nude.png",
		"red_fox.png",
		"robot.png",
		"runner.png",
		"space.png",
		"spaceship.png",
		"sweets_shop.png",
		"taco.png",
		"trash_panda.png",
		"two_lifes_big.png",
		"white_bird.png",
		"xmas_tree.png",
	],
	"generic_pixels_smol":[
		"backrooms_wallpaper.png",
		"blog_soft_icon.png",
		"camera.png",
		"desert.png",
		"desert_cake.png",
		"fishes_banner",
		"folk_jam.png",
		"gimp_forum_city.png",
		"pixels.png",
		"smiler.png",
		"sunset.png",
		"tree.png"
	],
	"high-spec":[
		"3d_discoball.png",
		"culture_palace.png",
		"gamepad_icon.png",
		"kenneys_3d_scene.png",
		"robot.png",
		"merry_xmas.jpeg",
		"street.png"
	],
	"lospec_dailies":[
		"amphibian.png",
		"ampibian_happy.png",
		"amphibian_wink.png",
		"book.png",
		"community.png",
		"dessert.png",
		"entertainer.png",
		"extinct.png",
		"goblin.png",
		"myth.png",
		"nostalgia_console.png",
		"nostalgia_flashplayer.png",
		"nostalgia_mario.png",
		"slime.png",
		"tool.png",
		"tower.png",
		"tower_anim.gif",
		"witch.png"
	],
	"other_gamedev":[
		"chara_template.png",
		"dudes.jpg",
		"user.png"
	],
	"tilesets":[
		"8c-tiles-platf.png",
		"generic_RPG.png",
		"generic_RPG_edit.png",
		"jam_lospec.png",
		"logic_3c.png",
		"master-tileset.png",
		"moji-font.png",
		"platformer_3c.png",
		"platformer_4c.png",
		"platformer_5c.png",
		"platformer_6c.png",
		"rpg_3c.png",
		"rpg_4c.png",
		"rpg_5c.png",
		"rpg_6c.png",
		"tileset_base.png",
		"tileset_rpg_big_af.png"
	],
	"wallpapers":[
		"bricks_horizontal_dark.png",
		"bricks_horizontal_light.png",
		"bricks_vertical_dark.png",
		"bricks_vertical_light.png",
		"checkerboard_dark_desktop.png",
		"checkerboard_dark_mobile.png",
		"checkerboard_light_desktop.png",
		"checkerboard_light_mobile.png",
		"friends_lined.png",
		"friends_pixel.png",
		"friends_stickerified.png",
		"starfield.png"
	]
}
